@extends('admin.master')

@section('body')
   
      <div class="main-content">
      	<h5 align="center">Manage access</h5><br/>
      	<b>{{ Session::get('message') }}</b>
		<table class="table table-hover talbe-striped table-bordered">
			<tr>
				<th>Sl No</th>
				<th>Name</th>
				<th>Regi Number</th>
				<th>Role</th>
				<th>Registration</th>
			@if(Auth::user()->role == 'Admin') 
				 <th>Action</th>
			@else
			     
			@endif     	 
			</tr>

       @php($i=1)
       @foreach($access as $value)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{ $value->name }}</td>
				<td>{{ $value->regi_number }}</td>
				<td>{{ $value->role }}</td>
				@if($value->active == 1)
					<td>Done</td>
				@else	
				    <td>No</td>
				@endif
			 
        @if(Auth::user()->role == 'Admin') 
			   <td>
			   	@if($value->active == 1 )
					<a href="#" onclick="return confirm('If you want to delete this user access ? Before you have to delete this user');"><i class="fa fa-trash"></i></a>
				@else
				    <a href="{{ route('deleteAccess',['id'=>$value->regi_number]) }}"><i class="fa fa-trash">
				@endif    	
				
			</td>  
        @else


        @endif
			</tr>
		@endforeach

		</table>
      </div>
      
@endsection('body')