@extends('admin.master')


@section('body')

	<div class="main-content">
		<h5 align="center">Admins</h5><br/>
		{{ Session::get('message') }}
		<table class="table table-hover table-striped table-bordered">
			<tr>
				<th>SL No</th>
				<th>Name</th>
				<th>Regi number</th>
				<th>Action</th>
			</tr>
        @php($i=1)
        @foreach($access as $value)
			<tr>
			   <th>{{ $i++ }}</th>
				<th>{{ $value->name }}</th>
				<th>{{ $value->regi_number }}</th>
				<th>
			<a href="{{ route('deleteStudent',['id'=>$value->id]) }}" onclick="return confirm('Delete this user after that you have to delete that user access')"><i class="fa fa-trash"></i></a>
				</th>
				</th>
			</tr>
        @endforeach
		</table>
	</div>

@endsection('body')