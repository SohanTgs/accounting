@extends('admin.master')

@section('body')
   <style type="text/css">

   </style>
      <div class="main-content">
      
      
      <form action="{{ route('saveAccess') }}" method="POST">   
      @csrf         
       <div class="row" style="width:330px;">
                <div class="col-md-12"> 
                  <h4 style="color:red;">{{ Session::get('message') }}</h4>
                  </div>  
                  <div class="col-md-12"> 
                   &nbsp;
                  </div>  
            	<div class="col-md-12">    
                   <h5> Allow to access here...</h5>
                  </div>
                  <div class="col-md-12"> 
                        &nbsp;
                  </div>      
                  <div class="col-md-12">
       			*<input type="nubmer" placeholder="Registration Number" name="regi_number" class="form-control">
       		</div>
       		<div class="col-md-12">	
       			&nbsp;
       		</div>
       		<div class="col-md-12">
       			*<input type="text" placeholder="Name" name="name" class="form-control">
       		</div>	
       		<div class="col-md-12">	
       			&nbsp;
       		</div>
       		<div class="col-md-12">
       			*<select name="role" class="form-control">
                              <option value="">---Select role---</option>
                        @if(Auth::user()->role == 'Admin')     
                              <option value="Admin">Admin</option>
                              <option value="Teacher">Teacher</option>
                              <option value="Student">Student</option> 
                        @else
                              <option value="Student">Student</option> 
                        @endif                       
                        </select>
       		</div>
                  <div class="col-md-12"> 
                        &nbsp;
                  </div>	
                  <div class="col-md-6"> 
                        <input type="submit" name="Submit" value="Submit" class="btn btn-success form-control">
                  </div>
               </div>
      </form>
      {{ $errors->has('regi_number')?$errors->first('regi_number'):'' }} <br/>
       {{ $errors->has('name')?$errors->first('name'):'' }} <br/>
        {{ $errors->has('role')?$errors->first('role'):'' }} 
      </div>
      
@endsection('body')