@extends('admin.master')

@section('body')
	<div class="main-content">
		<h5 align="center">Manage notice info...</h5><br/>
		<b style="color:black;">{{ Session::get('message') }}</b>
		<table class="table table-hover table-striped table-bordered">
			<tr>
				<th>SL No</th>
				<th>Heading</th>
				<th>Descripion</th>
				<th>Action</th>
			</tr>
			@php($i=1)
			@foreach($notice as $value)
			    <tr>
			       <td>{{ $i++ }}</td>
			       <td>{{ $value->heading }}</td>
			       <td>{{ $value->created_at }}</td>
			       <td>
			       	 <a href="{{ route('deleteNotice',['id'=>$value->id]) }}"><i class="fa fa-trash"></i></a>
			       	 <a href="#"><i class="fa fa-edit fa-2x"></i></a>
			       </td>
			    </tr>
		     @endforeach	    
		</table>
	</div>

@endsection('body')