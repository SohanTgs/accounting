<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/',[
	'uses'=>'Homepage@index',
	'as'=>'index'
]);

Route::get('/verify',[
	'uses'=>'Homepage@verify',
	'as'=>'verify'
]);

Route::get('/noticeBoard',[
	'uses'=>'Homepage@noticeBoard',
	'as'=>'noticeBoard'
]);

Route::get('/noticeBoard/showNotice/{id}',[
	'uses'=>'Homepage@showNotice',
	'as'=>'showNotice'
]);

Route::get('/onlyAdmin',[
	'uses'=>'Homepage@onlyAdmin',
	'as'=>'onlyAdmin'
]);

Route::POST('/onlyAdmin/onlyAdminSave',[
	'uses'=>'Homepage@onlyAdminSave',
	'as'=>'onlyAdminSave'
]);

Route::get('/ajax/{number}',[
	'uses'=>'Homepage@ajax',
	'as'  =>'ajax'
]);

Route::get('register2',[
	'uses'=>'Homepage@register2',
	'as'  =>'register2'
]);

Route::get('register2/ajax/{number}',[
	'uses'=>'Homepage@ajax',
	'as'  =>'ajax'
]);

Route::POST('/register2',[
	'uses'=>'Homepage@register2',
	'as'=>'register2'
]);

Route::POST('/register2/register3',[
	'uses'=>'Homepage@register3',
	'as'=>'register3'
]);




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['CheckUser']], function () {


Route::get('/home/addAllow',[
	'uses'=>'Homepage@addAllow',
	'as'=>'addAllow'
]);

Route::get('/home/manageAllow',[
	'uses'=>'Homepage@manageAllow',
	'as'=>'manageAllow'
]);

Route::POST('/home/addAllow/saveAccess',[
	'uses'=>'Homepage@saveAccess',
	'as'=>'saveAccess'
]);

Route::get('/home/newPost',[
	'uses'=>'Homepage@newPost',
	'as'=>'newPost'
]);

Route::POST('/home/newPost/savePost',[
	'uses'=>'Homepage@savePost',
	'as'=>'savePost'
]);


Route::get('/home/deletePost/{id}',[
	'uses'=>'Homepage@deletePost',
	'as'=>'deletePost'
]);

Route::get('/home/showPost/{id}',[
	'uses'=>'Homepage@showPost',
	'as'=>'showPost'
]);

Route::get('/home/newNotice',[
	'uses'=>'Homepage@newNotice',
	'as'=>'newNotice'
]);

Route::POST('/home/newNotice/saveNotice',[
	'uses'=>'Homepage@saveNotice',
	'as'=>'saveNotice'
]);

Route::get('/home/manageNotice',[
	'uses'=>'Homepage@manageNotice',
	'as'=>'manageNotice'
]);

Route::get('/home/manageNotice/deleteNotice/{id}',[
	'uses'=>'Homepage@deleteNotice',
	'as'=>'deleteNotice'
]);

Route::get('/home/deleteAccess/{id}',[
	'uses'=>'Homepage@deleteAccess',
	'as'=>'deleteAccess'
]);

Route::get('/home/admins',[
	'uses'=>'Homepage@admins',
	'as'=>'admins'
]);

Route::get('/home/treachers',[
	'uses'=>'Homepage@treachers',
	'as'=>'treachers'
]);

Route::get('/home/students',[
	'uses'=>'Homepage@students',
	'as'=>'students'
]);

Route::get('/home/students/deleteStudent/{id}',[
	'uses'=>'Homepage@deleteStudent',
	'as'=>'deleteStudent'
]);

Route::get('/home/profile',[
	'uses'=>'Homepage@profile',
	'as'=>'profile'
]);

Route::POST('/home/profile/updateUser',[
	'uses'=>'Homepage@updateUser',
	'as'=>'updateUser'
]);


}); 















