@extends('admin.master')

@section('body')
   <style type="text/css">
   	.row{
   		background: white;
   		  //  width: 800px;
   		  padding-top: 20px;
   		  padding-bottom: 20px;
   		}
    .frame{
    	border:none;
    }
   </style>
      <div class="main-content">  <h6 style="color:black;">{{ Session::get('message') }}</h6>
     @foreach($post as $value)  
     <br/>
       	<div class="row justify-content-center">
@if(Auth::user()->role == 'Admin')
   <a href="{{ route('deletePost',['id'=>$value->id]) }}"><i class="fa fa-trash"></i></a>
@else
   
@endif


@if(Auth::user()->role == 'Teacher' && $value->role == 'Student')
     <a href="{{ route('deletePost',['id'=>$value->id]) }}"><i class="fa fa-trash"></i></a>
@else
   
@endif             

            <div class="col-sm-6">
	         <div class="single-blog">
	         	<img src="{{ asset('user.png') }}" style="height: 50px;width: 50px;">
	         <span style="color:black;">{{ $value->created_at }}</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	        @if($value->regi_number == Auth::user()->regi_number) 	
	         	<a href="{{ route('deletePost',['id'=>$value->id]) }}"><i class="fa fa-trash"></i></a>&nbsp;&nbsp;&nbsp;
	         	<a href="#"><i class="fa fa-edit fa-2x"></i></a>
	        @else

	        @endif 	
	         	<br/>
	         	
	         	@if($value->regi_number == Auth::user()->regi_number)
	         	  by <b style="color:black;"> Me...({{ $value->role }})</b>
	         	@else	
	         	   by <b style="color:black;">{{ $value->name }} ({{ $value->role }})</b>
	         	@endif    
	         	</b>
	         	<br/>
	         	@if($value->subject == Null)

	         	@else    
	             	( {{ $value->subject }} )
	            @endif 	
	         	<br/>
	         <div class="blog-thumb">
	        @if($value->post_picture == Null) 
	        	
	        @else
	        <iframe src="{{ asset($value->post_picture) }}" height="250px" class="frame"></iframe> 
	            <a href="{{ route('showPost',['id'=>$value->id]) }}"><i class="fa fa-eye"></i></a>  	
	        @endif 
	         </div><br/>

	         <p style="color:black;">
	         	{{ $value->post_write }}
	         </p>
	          </div>
	         </div>
	         <div>
	         	<input type="submit" name="btn" value="Comments..." class="btn btn-dark">
	         </div>
       	</div>
	<br/>
     @endforeach  
      </div>
      
@endsection('body')