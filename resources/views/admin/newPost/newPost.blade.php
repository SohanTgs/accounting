@extends('admin.master')

@section('body')
   
      <div class="main-content">
         <form action="{{ route('savePost') }}" method="POST" enctype= multipart/form-data>
         	@csrf
         	<div class="row">
                <h5 align="center">New post write here...</h5>
                <div class="col-md-12">
                <h4 style="color:red;">{{ Session::get('message') }}</h4>
                </div>
               <div class="col-md-12">
                 &nbsp;
                </div>

         		<div class="col-md-12">
         			<textarea name="post_write" maxlength="250" style="height: 150px;width: 300px" placeholder="Say something here........."></textarea>
         		</div>
         		<input type="hidden" name="regi_number" value="{{ Auth::user()->regi_number }}">
         		<input type="hidden" name="name" value="{{ Auth::user()->name }}">
         		<input type="hidden" name="subject" value="{{ Auth::user()->subject }}">
                <input type="hidden" name="role" value="{{ Auth::user()->role }}">
         		<div class="col-md-12">
                 &nbsp;
         		</div>
         		<div class="col-md-12">
         		<label for='post_picture'>Picture or file</label>
                 <input type="file" name="post_picture" accept="*">
         		</div>
         		<div class="col-md-12">
                 &nbsp;
         		</div>
         		<div class="col-md-2">
                 <input type="submit" name="submit" value="Submit post" class="btn btn-dark">
         		</div>
         	</div>
         </form>		
      </div>
      
@endsection('body')