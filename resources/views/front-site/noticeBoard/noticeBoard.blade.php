@extends('front-site.master')

@section('body')
<br/><br/><br/><br/>
<h3 align="center">Department of accounting 2018-2019</h3>
<br/><br/>
     <div class='row justify-content-center' style="background: ;">
     	<div class="col-md-6">
                         @php($i=1)
          @foreach($notice as $value)	
          <div class="box" style="background: #F6F6F6;">	
     		<table class="table">
     			<tr>
     				<td><p><b style="color:black;">
     		 {{$i++}}. &nbsp; {{ $value->heading }}
     					</b></p></td>
     					<td></td>
     			</tr>
     		    <tr>
     			<td><p style="color: black;">{{ $value->created_at }}
                         &nbsp;&nbsp;by <b>{{ $value->name }}</b> ({{ $value->role }})
                    </p></td>
     			<td></td>
     			</tr>
     			<tr>
     			@if(!$value->notice_r == Null)
                         <td><span style="color: black;">{{ $value->notice_r }}</span></td>
     				<td></td>
                    @else
                         
                    @endif   
     			</tr>
          		<tr>
                    @if(!$value->notice == Null)
          			<td><a href="{{ route('showNotice',['id'=>$value->id]) }}" class="btn btn-dark btn-sm">View notice</a></td>
          			<td></td>
                    @else
                          
                    @endif            
     			</tr>

     		</table>
     		</div>	
                @endforeach 
     	</div>
     </div>
<br/><br/><br/>
@endsection('body')