@extends('admin.master')

@section('body')
  
<div class="main-content">
<h5>{{ Session::get('message') }}</h5>
 @if(Auth::user()->role == 'Teacher' or (Auth::user()->role == 'Admin'))
       <form action="{{ route('updateUser') }}" method="POST">
       	@csrf
       		<input type="text" name="subject" value="{{ Auth::user()->subject }}" placeholder="Subject">
       		<input type="hidden" name="id" value="{{ Auth::user()->id }}">
       		<input type="submit" name="btn" value="Submit" class="btn btn-dark">
       </form>
 @else 

 @endif
</div>

@endsection('body')