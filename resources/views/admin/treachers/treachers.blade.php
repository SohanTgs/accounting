@extends('admin.master')


@section('body')

	<div class="main-content">
		<h5 align="center">Teachers</h5><br/>
		{{ Session::get('message') }}
		<table class="table table-hover table-striped table-bordered">
			<tr>
				<th>SL No</th>
				<th>Name</th>
				<th>Subject</th>
				<th>Regi number</th>
			@if(Auth::user()->role == 'Admin')	
				<th>Action</th>
            @else

            @endif      
			</tr>
        @php($i=1)
        @foreach($access as $value)
			<tr>
			   <th>{{ $i++ }}</th>
				<th>{{ $value->name }}</th>
				<th>{{ $value->subject }}</th>
				<th>{{ $value->regi_number }}</th>
			@if(Auth::user()->role == 'Admin')	
				<th>
			 <a href="{{ route('deleteStudent',['id'=>$value->id]) }}" onclick="return confirm('Delete this user after that you have to delete that user access')"><i class="fa fa-trash"></i></a>
				</th>
             @else
                     
             @endif 
			</tr>
        @endforeach
		</table>
	</div>

@endsection('body')