<!DOCTYPE html>
<html lang="en">


<!-- index.html  21 Nov 2019 03:44:50 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Dashboard </title>
  <link rel="icon" href="{{ asset('/') }}/title.png">
  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('/') }}/admin/css/app.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('/') }}/admin/css/style.css">
  <link rel="stylesheet" href="{{ asset('/') }}/admin/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="{{ asset('/') }}/admin/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href="{{ asset('/') }}/admin/img/favicon.ico"/>
</head>

<body>
  <div class="loader"></div>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar sticky">
        <div class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg
									collapse-btn"> <i data-feather="align-justify"></i></a></li>
            <li><a href="#" class="nav-link nav-link-lg fullscreen-btn">
                <i data-feather="maximize"></i>
              </a></li>
            <li>
              <form class="form-inline mr-auto">
                <div class="search-element">
                  <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="200">
                 <!--  <button class="btn" type="submit">
                   <i class="fas fa-search"></i>
                  </button>-->
                </div>
              </form>
            </li>
          </ul>
        </div>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown dropdown-list-toggle"> <!--<a href="#" data-toggle="dropdown"
              class="nav-link nav-link-lg message-toggle"><i data-feather="mail"></i>
              <span class="badge headerBadge1">
                 </span> </a>-->
          
          </li>
        
          <li class="dropdown"><a href="#" data-toggle="dropdown"
              class="nav-link dropdown-toggle nav-link-lg nav-link-user"> <img alt="image" src="{{ asset('user.png') }}"
                class="user-img-radious-style"> <span class="d-sm-none d-lg-inline-block"></span></a>
            <div class="dropdown-menu dropdown-menu-right pullDown">
            <div class="dropdown-title">{{ Auth::user()->name }}</div>
              <a href="{{ route('profile') }}" class="dropdown-item has-icon"> <i class="far
										fa-user"></i> Profile
              </a> 
              <a href="#" class="dropdown-item has-icon"> <i class="fas fa-cog"></i>
                Settings
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       <i class="fas fa-sign-out-alt"></i>  {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
 <h3>{{ Auth::user()->role }}</h3>
         <p>Tgs...</p>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">Main</li>
            <li class="dropdown active">
              <a href="{{ url('/home') }}" class="nav-link"><i data-feather="monitor"></i><span>Dashboard</span></a>
            </li>
         @if(Auth::user()->role == 'Teacher' or (Auth::user()->role == 'Admin'))
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="briefcase"></i><span>Allow to access</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ route('addAllow') }}">Add allow</a></li>
                <li><a class="nav-link" href="{{ route('manageAllow') }}">Manage allow</a></li>
              </ul>
            </li>
         @else 
                
         @endif         
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="command"></i><span>Create Post</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ route('newPost') }}">New Post</a></li>
                 <li><a class="nav-link" href="chat.html">My posts</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i class="fa fa-edit"></i><span>Notice info</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ route('newNotice') }}">Add new notice</a></li>
                <li><a class="nav-link" href="{{ route('manageNotice') }}">Manage notice</a></li>
              </ul>
          </li>
       <li class="menu-header">Users info</li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="copy"></i><span>Control users</span></a>
              <ul class="dropdown-menu">
              @if(Auth::user()->role == 'Admin')  
                  <li><a class="nav-link" href="{{ route('admins') }}">Admins</a></li>
                  <li><a class="nav-link" href="{{ route('treachers') }}">Teachers</a></li>
                  <li><a class="nav-link" href="{{ route('students') }}">Studets</a></li>
              @elseif(Auth::user()->role == 'Teacher')
                  <li><a class="nav-link" href="{{ route('treachers') }}">Teachers</a></li>
                  <li><a class="nav-link" href="{{ route('students') }}">Studets</a></li>
              @else
                  <li><a class="nav-link" href="{{ route('students') }}">Studets</a></li>   
              @endif 
              </ul>
      <!--      </li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i
                  data-feather="shopping-bag"></i><span>Advanced</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="avatar.html">Avatar</a></li>

              </ul>
            </li>
            <li><a class="nav-link" href="blank.html"><i data-feather="file"></i><span>Blank Page</span></a></li>
            <li class="menu-header">Otika</li>
            <li class="dropdown">
              <a href="#" class="menu-toggle nav-link has-dropdown"><i data-feather="layout"></i><span>Forms</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="basic-form.html">Basic Form</a></li>

              </ul>
            </li> -->
        
          </ul>
        </aside>
      </div>

@yield('body')

<footer class="main-footer">
        <div class="footer-left">
          <a href="templateshub.net">Templateshub</a></a>
        </div>
        <div class="footer-right">
        </div>
      </footer>
    </div>
  </div>
  <!-- General JS Scripts -->
  <script src="{{ asset('/') }}/admin/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="{{ asset('/') }}/admin/bundles/apexcharts/apexcharts.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="{{ asset('/') }}/admin/js/page/index.js"></script>
  <!-- Template JS File -->
  <script src="{{ asset('/') }}/admin/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="{{ asset('/') }}/admin/js/custom.js"></script>
</body>


<!-- index.html  21 Nov 2019 03:47:04 GMT -->
</html>