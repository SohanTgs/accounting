<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Access;
use App\Post;
use App\Notice;
use App\User;




class Homepage extends Controller
{
    
	public function index(){
		return view('front-site.homepage.homepage');
	}

	public function addAllow(){
		return view('admin.addAllow.addAllow');
	}
	
	public function manageAllow(){
           $teacher = Access::all();
           $access = Access::all();
		return view('admin.manageAllow.manageAllow',['access'=>$access,'teacher'=>$teacher]);
	}

	public function saveAccess(Request $request){
		  $this->validate($request,[
		      'regi_number'=>'required',
		      'name'=>'required',
		      'role'=>'required'
		    ]);

		$access = new Access();
		$access->regi_number = $request->regi_number;
		$access->name = $request->name;
		$access->role = $request->role;
		$access->active = 0;
		$access->save();
		return redirect('home/addAllow')->with('message','Allowed successfully');
	}

	public function verify(){
		return view('front-site.verify.verify');
	}

	public function newPost(){
		return view('admin.newPost.newPost');
	}

    public function savePost(Request $request){

		$post = new Post();
		$post->regi_number = $request->regi_number;
		$post->name = $request->name;
		$post->subject = $request->subject;
		$post->post_write = $request->post_write;
		$post->role = $request->role;
   	if($request->hasFile('post_picture')){
	   	    $picture = $request->file('post_picture');
			$Name = $picture->getClientOriginalName();
			$directory = 'Posts/';
			$ImageUrl = $directory.$Name;
			$picture->move($directory,$Name);
        $post->post_picture = $ImageUrl;
   		}else{
   			$post->post_picture = $request->post_picture;
   		}
		$post->save();
		return redirect('home/newPost')->with('message','Your post uploaded');
			
	}

	public function deletePost($id){
        $post = Post::find($id);
        $post->delete();
        return redirect('home')->with('message','Post deleted successfully');
    }

    public function showPost($id){
    	$id = Post::find($id);
    	return view('admin.showPost.showPost',['id'=>$id]);
    }

    public function noticeBoard(){
    	$notice = Notice::orderBy('id','desc')->get();
    	return view('front-site.noticeBoard.noticeBoard',['notice'=>$notice]);
    }
 
    public function newNotice(){
    	return view('admin.newNotice.newNotice');
    }
	
	public function saveNotice(Request $request){
		$this->validate($request,[
			'heading'=>'required'
		]);
		$notice = new Notice();
		$notice->name = $request->name;
		$notice->role = $request->role;
		$notice->heading = $request->heading;
	if($request->hasFile('notice')){
	   	    $picture = $request->file('notice');
			$Name = $picture->getClientOriginalName();
			$directory = 'Notice/';
			$ImageUrl = $directory.$Name;
			$picture->move($directory,$Name);
        $notice->notice = $ImageUrl;
	}else{
       $notice->notice = $request->notice;
	}
		
		$notice->notice_r = $request->notice_r;
		$notice->save();
		return redirect('home/newNotice')->with('message','Notice published successfully');
	}   

    public function showNotice($id){
    	$id = Notice::find($id);
    	return view('front-site.showNotice.showNotice',['id'=>$id]);
    }

    public function manageNotice(){
    	$notice = Notice::orderBy('id','desc')->get();
    	//return $notice;
    	return view('admin.manageNotice.manageNotice',['notice'=>$notice]);
    }

    public function deleteNotice($id){
        $notice = Notice::find($id);
        $notice->delete();
        return redirect('home/manageNotice')->with('message','Notice deleted successfully');
    }

    public function deleteAccess($id){
        $access = Access::where('regi_number',$id)->first(); 
        $access->delete();
    	return redirect('home/manageAllow')->with('message','Canceled access successfully');
    }

    public function admins(){
    	$access = User::where('role','Admin')->get();
    	return view('admin.admins.admins',['access'=>$access]);
    }

    public function treachers(){
    	$access = User::where('role','Teacher')->get();
    	return view('admin.treachers.treachers',['access'=>$access]);
    }

    public function students(){
    	$access = User::where('role','Student')->get();
    	return view('admin.students.students',['access'=>$access]);
    }

    public function deleteStudent($id){
    	$id = User::find($id);
    	$access = Access::where('regi_number',$id->regi_number)->first();
    	$access->active = 0;
    	$access->save();
    	$id->delete();
    	return redirect('home/students')->with('message','Users deleted successfully');
    }

    public function profile(){
    	return view('admin.profile.profile');
    }

    public function updateUser(Request $request){
    	$user = User::find($request->id);
    	$user->subject = $request->subject;
    	$user->save();
    	return redirect('home/profile')->with('message','Users updated successfully');
    }

    public function onlyAdmin(){
    	$access = Access::count();
    	if($access == 0){
    		return view('admin.only.only');
    	}
    	 else{
    	 	return 'Thank your...';
    	 }
    	

    }

    public function onlyAdminSave(Request $request){
      $access = new Access();
      $access->name = $request->name;
      $access->regi_number = $request->regi_number;
      $access->role = $request->role;
      $access->active = 0;
      $access->save();
    	return redirect('/');
    }

    public function ajax($number){
        $access = Access::where('regi_number',$number)->first();
            if($access){
                 return $access->name;
             }else{
                 return '';
         }
   }

   public function register2(Request $request){
    $this->validate($request,[
      'regi_numebr'=>'required'
    ]);
   
        if($_GET['regi_numebr']){
         $number = $_GET['regi_numebr'];
         $access = Access::where('regi_number',$number)->first();
         if($access){
              if($access->active == 1){
                     return 'Already you have registered. Thank you .....';
              }else{
                return view('front-site.register2.register2',['access'=>$access,'number'=>$number]);
              }
         }else{
            return "Sorry, We can't find this registration number.....";
         }
        
        }else{
            return "There isn't registration number .....";
        }
        
    }

 

    public function register3(Request $request){
      $this->validate($request,[
            'regi_number'=>"required|unique:users",
            'name' => 'required|string|max:255',
            'email' => 'required |string |email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed'
        ]);
          $number = $request->regi_number;
          $access = Access::where('regi_number',$number)->first();
          $access->active = 1;
          $access->save();

          $user = new User();
          $user->name = $request->name;
          $user->email = $request->email;
          $user->password = bcrypt($request->password);
          $user->role = $access->role;
          $user->regi_number = $access->regi_number;
          $user->save();

          return redirect('/login')->with('message','Your registration done...');


    }








}


