@extends('front-site.master')

@section('body')

    <!-- banner -->
    <div class="main-w3pvt mian-content-wthree text-center" id="home">
        <div class="container">
            <div class="style-banner mx-auto">
                <h3 class="text-wh font-weight-bold">Welcome to <span>My School</span> <br>Best for Education</h3>
                <p class="mt-3 text-li" id="join"><b>Department of accounting 2018-2019</b></p>
                <!-- form -->
                <div class="home-form-w3ls mt-5">
                    
                </div>
                <!-- //form -->
            </div>
        </div>
    </div>
    <!-- //banner -->
    <section class="w3ls-bnrbtm py-5" id="about">
        
    </section>

   <!--
    <section class="blog_w3ls py-5" id="events">
        <div class="container py-xl-5 py-lg-3">
            <h3 class="title-w3 mb-5 text-center font-weight-bold">Don't Miss Our Event</h3>
            <div class="row mt-4">
                <div class="col-lg-4">
                    <div class="wthree-title mt-lg-5 pt-lg-3">
                        <h3 class="w3pvt-title text-bl">Sint occ aecat cupi</h3>
                        <p class="border-top pt-4 mt-4">
                            Donec consequat sapien ut leo cursus rhoncus. Nullam dui mi, vulputate ac metus at,
                            semper varius orci. Nulla accumsan ac
                            elit in congue.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row mt-lg-4">

 
                <div class="col-lg-4 col-md-6 mt-lg-0 mt-5">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="blog-title card-title m-0">
                                <a href="single.html">Cras ultricies ligula sed.</a>
                            </h5>
                        </div>
                        <div class="card-body">
                            <p class="text-left">Proin eget tortor risus. Curabitur aliquet quam id dui posuere
                                blandit. Vivamus
                                magna justo,
                                lacinia eget consectetur sed, convallis at tellus. Vestibulum ac diam sit.</p>
                            <a class="service-btn mt-xl-5 mt-4 btn" href="#">Read More<span class="fa fa-long-arrow-right ml-2"></span></a>
                        </div>
                        <div class="card-footer blog_w3icon border-top pt-2 mt-3 d-flex justify-content-between">
                            <small class="text-bl">
                                <b>By: Loremipsum</b>
                            </small>
                            <span>
                                March 28,2019
                            </span>
                        </div>
                    </div>
                </div>

              
            </div>
        </div>
    </section>
     -->

    <!-- gallery -->
    
    <!-- //gallery -->

    <!-- testimonials -->
    
    <!-- //testimonials -->

    <!-- apps -->

    <!-- //apps -->

    <!-- contact-->
    <section class="contact py-5" id="contact">
        <div class="container py-xl-5 py-lg-3">
            <h3 class="title-w3 mb-sm-5 mb-4 text-center text-wh font-weight-bold">Contact Us</h3>
            <div class="contact_grid_right pt-4">
                <form action="#" method="">
                    <div class="row contact_left_grid">
                        <div class="col-lg-6 con-left" data-aos="fade-up">
                            <div class="form-group">
                                <input class="form-control" type="text" name="Name" placeholder="Name" required="">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="email" name="Email" placeholder="Email" required="">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="Subject" placeholder="Subject" required="">
                            </div>
                        </div>
                        <div class="col-lg-6 con-right" data-aos="fade-up">
                            <div class="form-group">
                                <textarea id="textarea" placeholder="Message" required=""></textarea>
                            </div>
                            <div class="sub-honey">
                                <button class="form-control btn" type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- //contact -->

    <!-- footer -->
    
    <!-- copyright -->
    <div class="copyright-w3ls py-4">
        <div class="container">
            <div class="row">
                <!-- copyright -->
                <p class="col-lg-8 copy-right-grids text-wh text-lg-left text-center mt-lg-2">© 2020 My School. | Designed by & developed Mohammad Sohan Tgs
                </p>
                <!-- //copyright -->
                <!-- social icons -->
                <div class="col-lg-4 w3social-icons text-lg-right text-center mt-lg-0 mt-3">
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/tgs.sohan" target="_blank" class="rounded-circle">
                                <span class="fa fa-facebook-f"></span>
                            </a>
                        </li>
                        <li class="px-2">
                            <a href="https://www.instagram.com/mohammadsohan1" target="_blank" class="rounded-circle">
                                <span class="fa fa-instagram"></span>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/Mohamma96574769" target="_blank" class="rounded-circle">
                                <span class="fa fa-twitter"></span>
                            </a>
                        </li>
                        
                    </ul>
                </div>
                <!-- //social icons -->
            </div>
        </div>
    </div>
    <!-- //copyright -->
    <!-- move top icon -->
    <a href="#home" class="move-top text-center">
        <span class="fa fa-angle-double-up" aria-hidden="true"></span>
    </a>
    <!-- //move top icon -->

</body>

</html>
@endsection('body')