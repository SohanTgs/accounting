@extends('admin.master')

@section('body')
   
      <div class="main-content">
         <form action="{{ route('saveNotice') }}" method="POST" enctype= multipart/form-data>
         	@csrf
         	<div class="row">
                <h5 align="center">Notice info here...</h5>
                <div class="col-md-12">
                <h4 style="color:red;">{{ Session::get('message') }}</h4>
                </div>
               <div class="col-md-12">
                 &nbsp;
                </div>
              *<input type="text" name="heading" maxlength="250" placeholder="Notice heading" class="form-control" style="width: 299px;
                    margin-left: 15px;">
                 <div class="col-md-12">
                     &nbsp;
                 </div>   
         		<div class="col-md-12">
         			<textarea name="notice_r" maxlength="250" style="height: 150px;width: 300px" placeholder=" notice description........."></textarea>
         		</div>
         		<input type="hidden" name="name" value="{{ Auth::user()->name }}">
                <input type="hidden" name="role" value="{{ Auth::user()->role }}">
         		<div class="col-md-12">
                 &nbsp;
         		</div>
         		<div class="col-md-12">
         		<label for='notice'>Notice board</label>
                 <input type="file" name="notice" accept="*">
         		</div>
         		<div class="col-md-12">
                 &nbsp;
         		</div>
         		<div class="col-md-2">
                 <input type="submit" name="submit" value="Submit post" class="btn btn-dark">
         		</div>
         	</div>
         </form>
         {{ $errors->has('heading')?$errors->first('heading'):'' }} 		
      </div>
      
@endsection('body')