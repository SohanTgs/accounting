<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {    $post = Post::orderBy('id','desc')->get();
        // $teacherPower = Post::where('role','Student')->get();
        return view('admin.admin.admin',['post'=>$post]);
    }
}
